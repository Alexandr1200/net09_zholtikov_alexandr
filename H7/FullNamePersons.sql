USE [Net09 P1]
GO
CREATE PROC FullNamePersons 
@LastName VARCHAR(50)
AS

BEGIN
	SELECT @LastName AS FirstName, LastName, Patronymic
	FROM Persons
	WHERE EXISTS (SELECT LastName FROM Persons WHERE LastName = @LastName) 
	AND Persons.LastName = @LastName
END;