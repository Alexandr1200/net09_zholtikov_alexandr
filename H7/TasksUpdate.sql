USE [Net09 P1]
GO
CREATE PROC TasksUpdate 
@description VARCHAR(50),
@ID INT 
AS
BEGIN
	UPDATE Tasks 
	SET Description = @description
	WHERE Tasks.ID = @ID
END;