﻿using System;
using Db.Model;
using Db;

namespace Program
{   
    public class Program
    {
        public static void Control(string value, string text)
        {
            while (string.IsNullOrEmpty(value))
            {
                Console.WriteLine("Данное поле не может быть пустым");
                Console.Write(text);
                value = Console.ReadLine();
            }
            return;
        }

        public static void Main(string[] args)
        {
            for (int i = 0; i < i + 1; i++)
            {
                Console.WriteLine("--Для выхода нажмите ESC--");
                Console.WriteLine("--Чтобы продолжить нажмите Enter--");
                var key = Console.ReadKey();
                if (key.Key == ConsoleKey.Escape)
                {
                    break;
                }
                using var context = new AutoDbContext1();

                string text;

                Console.Write(text = "Введите своё имя - ");
                var firstname = Console.ReadLine();
                Control(firstname, text);

                Console.Write(text = "Введите свою фамилию - ");
                var lastname = Console.ReadLine();
                Control(lastname, text);

                Console.Write(text = "Введите производителя автомобиля - ");
                var manufacture = Console.ReadLine();
                Control(manufacture, text);

                Console.Write(text = "Введите модель автомобиля - ");
                var model = Console.ReadLine();
                Control(model, text);

                var client = new Client
                {
                    FirstName = firstname,
                    LastName = lastname
                };

                var car = new Car
                {
                    Client = client,
                    Manufacture = manufacture,
                    Model = model
                };

                context.Clients.Add(client);
                context.Cars.Add(car);
                context.SaveChanges();

                Console.WriteLine();
            }
        }
    }
}
