﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace Db.Model
{
    public class SampleContextFactory : IDesignTimeDbContextFactory<AutoDbContext1>
    {
        public AutoDbContext1 CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AutoDbContext1>();

            
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("AutoDbJson.json");
            IConfigurationRoot config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(connectionString, opts => opts.CommandTimeout((int)TimeSpan.FromMinutes(10).TotalSeconds));
            return new AutoDbContext1(optionsBuilder.Options);
        }
    }
}
