﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Db.Model
{
	public class Task
	{
		[Key]
		public int Id { get; set; }
		public int ParentID { get; set; }
		
		[ForeignKey("UserId")]
		public int UserID { get; set; }	
		public virtual User User { get; set; }

		[Required]
		[MaxLength(20)]
		public string Title { get; set; }

		[Required]
		[Column (TypeName = "text")]
		public string Description { get; set; }
		public bool IsCompleated { get; set; }
		public bool IsInProgress { get; set; }
		public bool IsPending { get; set; }
        public int TaskPriorityId { get; set; }

    }
}
