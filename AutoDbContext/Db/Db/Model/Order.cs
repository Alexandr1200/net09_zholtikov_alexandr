﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Db.Model
{
	public class Order
	{
		[Key]
		public int Id { get; set; }

		[ForeignKey("ClientId")]
		public int ClientID { get; set; }
		public virtual Client Client { get; set; }

		public int TaskID { get; set; }
	}
}
