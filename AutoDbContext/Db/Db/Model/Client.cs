﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Db.Model
{
	public class Client
	{
		[Key]
		public int Id { get; set; }
		public virtual ICollection<Order> Orders { get; set; }

		[Required]
		[MaxLength(20)]
		public string FirstName { get; set; }
		[Required]
		[MaxLength(20)]
		public string LastName { get; set; }
		[MaxLength(20)]
		public int PhoneNumber { get; set; }
	}
}
