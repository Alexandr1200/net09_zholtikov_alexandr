﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Db.Model
{
	public class Car
	{	
		[Key]
		public int Id { get; set; }
		[Required]
		[MaxLength(10)]
		public int Number { get; set; }

		[ForeignKey("ClientID")]
		public int ClientID { get; set; }
		public virtual Client Client { get; set; }

		[Required]
		[MaxLength(20)]
		public string Model { get; set; }
		[MaxLength(20)]
		public string Manufacture { get; set; }
		[Required]
		[MaxLength(20)]
		public string VIN { get; set; }
	}
}
