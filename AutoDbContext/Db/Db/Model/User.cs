﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Db.Model
{
	public class User
	{
		[Key]
		public int Id { get; set; }
		public virtual ICollection<Task> Tasks { get; set; }

		[Required]
		[MaxLength(20)]
		public string Login { get; set; }
		[Required]
		[MaxLength(20)]
		public int Password { get; set; }
	}
}
