﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Db.Model
{
    public class TaskPriority
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(20)]
        public string Priority { get; set; }
    }
}
