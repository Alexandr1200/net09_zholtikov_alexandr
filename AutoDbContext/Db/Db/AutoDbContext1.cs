﻿using Db.Model;
using Microsoft.EntityFrameworkCore;


namespace Db
{
	public class AutoDbContext1 : DbContext
	{
		public DbSet<User> Users { get; set; }
		public DbSet<Task> Tasks { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Car> Cars { get; set; }
		public DbSet<Client> Clients { get; set; }
		public DbSet<TaskPriority> TaskPriorities { get; set; }
		public AutoDbContext1(DbContextOptions<AutoDbContext1> options): base(options)
        {
			Database.EnsureCreated();
			Database.EnsureDeleted();
		}
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			
			optionsBuilder.LogTo(message => System.Diagnostics.Debug.WriteLine(message));
		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().HasAlternateKey(user => user.Login);
			modelBuilder.Entity<User>().HasAlternateKey(user => user.Password);
			modelBuilder.Entity<Car>().HasAlternateKey(car => car.VIN);
			modelBuilder.Entity<Car>().HasAlternateKey(car => car.Number);

		}
	}
}
